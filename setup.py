#!/usr/bin/env python

from setuptools import setup, find_packages

__version__ = '0.0.0'
__program__ = 'kanele'

setup(
    name=__program__,
    version=__version__,
    description='',
    author='Noel Martignoni',
    author_email='noel@martignoni.fr',
    url='https://gitlab.com/{0}/{0}'.format(__program__),
    scripts=['scripts/{}'.format(__program__)],
    install_requires=['GitPython', 'termcolor', 'karamel', 'request'],
    packages=find_packages(exclude=['tests*']),
)
